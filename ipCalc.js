var createIpCalc = function(){
	
	var self = Object.create(Object);	

	/**
	* Creates ArrayBuffer to hold ip bits.
	*
	* @arg {number} Numer that represents IP address
	* @return {Unit32Array} 
	*/
	var _makeIp = function(val){
		var val = val?val:0;
		ip = new Uint32Array(new ArrayBuffer(4));
		ip[0] = val;
		return ip;
	}

	/**
	* Validates IP string and creates Unit32Array representaion of it.
	*
	* @throws {string} exception on octet ovefflow ip format.
	*
	* @arg {string} IP octets divided by dots
	* @return {Unit32Array} IP representation
	*/
	var _parseIp = function(ipString){

		if(typeof ipString !== 'string' || ipString.search(/^(\d{1,3}\.){3}(\d{1,3})$/) === -1){
			throw 'Not valid ip addres format.';
		}

		ip = _makeIp();
		ipArr = ipString.split('.');
		ipArr.map(function(el, i){
			var ipOctet = parseInt(el)
			if(ipOctet<256){
				ip[0] += (ipOctet << (3-i)*8);
			}else{
				throw 'IP octet overflow';
			}
		});

		return ip;
	}
	self.parseIp = _parseIp; 


	/**
	* Inverse function from _parseIp, creates string from byte value.
	*
	* @arg {ArrayBuffer} Holds ip bytes
	* @return {string} Ip addres on string fromat
	*/
	var _ipToString = function(ip){

		var ipMask =_parseIp('255.0.0.0');
		var ipArr =[];
		var tempIp = _makeIp();
		for(var i=0; i<4; i++){
			tempIp[0] = (((ipMask[0] >>> i*8) & ip[0]) >>> (3-i)*8);
			ipArr.push(tempIp[0]); 
		}

		return ipArr.join('.');
	}
	self.ipToString = _ipToString

	
	/**
	* Calculates brodband IP.
	*
	* @arg {ArrayBuffer} net IP
	* @arg {ArrayBuffer} IP mask
	* @return {ArrayBuffer} brodband IP
	*/
	var _calcBroadIp = function(netIp, mask){return _makeIp(netIp[0]&mask[0] | ~mask[0])}


	
	/**
	* Calculates default NetworkIp IP.
	*
	* @arg {ArrayBuffer} network IP
	* @arg {ArrayBuffer} IP mask
	* @return {ArrayBuffer}  IP
	*/
	var _calcNetIp = function(netIp, mask){return _makeIp(netIp[0] & mask[0]);}

	/**
	* Calculates network ranges NetworkIp IP.
	*
	* @arg {ArrayBuffer} default network IP
	* @arg {ArrayBuffer} IP mask
	* @return {Object} {topClientIp, bottomClientIp, nClien}
	*/
	var _calcRange = function(defNetIp, broadcastIp){
		var top = broadcastIp;
		var bottom = defNetIp;
		var rangeNum = top[0]-bottom[0]-1;
		return {topClienIp: _makeIp(top[0]-1), bottomClientIp: _makeIp(bottom[0]+1), nCLients: rangeNum}
	}

	/**
	* Calcultes subnet parametars.
	*
	* @arg {string} network ip
	* @arg {string} mask ip
	* @returns {Object} {netIp, subnetMask, broadIp, maxCliIp, minCliIp, possbileClients}
	*/
	var _calcNetParams = function(ipStr, maskStr){
		try{
			var mask  	= _parseIp(maskStr);
			var netIp 	= _calcNetIp(_parseIp(ipStr), mask);
			var broadIp = _calcBroadIp(netIp, mask);
			var range   = _calcRange(netIp, broadIp);
			return {
				netIp : 		_ipToString(netIp),
				subnetMask : 	_ipToString(mask),
				broadIp : 		_ipToString(broadIp),
				maxCliIp:  _ipToString(range.topClienIp),
				minCliIp: _ipToString(range.bottomClientIp),
				possibleClients: range.nCLients
			};
		}
		catch(e){
			return false;
		}
	}
	self.calcNetParams = _calcNetParams;

	/**
	* Checks if client address exists in subnets IP-space.
	*
	* @param {string} Network IP
	* @param {string} Mask IP
	* @param {string} Client IP
	*/
	var _isNetClient = function(netIpStr, maskStr, clientIpStr){
		var cIp = _parseIp(clientIpStr);
		var mask = _parseIp(maskStr);
		var nIp = _calcNetIp(_parseIp(netIpStr), mask);
		var bIp = _calcBroadIp(nIp, mask);

		return  (cIp[0] !== nIp[0]) && (cIp[0] !== bIp[0]) && ((cIp[0] - nIp[0]) ===  (~mask[0] & cIp[0])); 
	}
	self.isNetClient = _isNetClient;



	/**
	* Needs some additional testing.
	* It calculates network params so min and max fit in clients IP range. Can modify low/high client ip by one. 
	* Does not check if min ip is really lower than max ip.
	*
	* @param {string} Min client Ip
	* @parma {string} Max client Ip
	* @return {object} Look @_calcNetParams
	*/
	var _findOptSubnet =function(minIpStr, maxIpStr){
		var minIp = _parseIp(minIpStr);
		var maxIp = _parseIp(maxIpStr);

		var mask = _makeIp(~_makeIp((maxIp[0] ^ minIp[0])));
		
		var filter = _makeIp(Math.pow(2,31))
		var test = _makeIp(1);
		while(test[0] !== 0){
			test = _makeIp(filter[0] & mask[0]);
			filter = _makeIp(filter >>> 1);
		}
		mask[0] = _makeIp(mask[0] & _makeIp(~_makeIp( Math.pow(2, Math.sqrt(filter[0]))-1)[0]));


		var netIp = mask[0] && minIp;  
		return _calcNetParams(_ipToString(netIp), _ipToString(mask));

	}
	self.findOptSubnet = _findOptSubnet;	

	return self;

}


var ipCalc = createIpCalc();



